
let diceRecord = { "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0, "12": 0 };

function rollAndRecord() {
  for (let i = 1; i <= 1000; i++) {
    const diceOne = Math.floor(Math.random() * 6 + 1);
    const diceTwo = Math.floor(Math.random() * 6 + 1);
    const result = diceOne + diceTwo;
    diceRecord[result + ""]++;
  }
}

function printDiceRollRecord() {
  const possibleNumbers = Object.keys(diceRecord);
  let printH2 = document.createElement("h2");
  let headerText = document.createTextNode("Roll Frequency");
  printH2.appendChild(headerText);
  document.body.appendChild(printH2);

  for (let x = 0; x < possibleNumbers.length; x++) {
    let roll = document.createTextNode(possibleNumbers[x] + ":");
    document.body.appendChild(roll);
    rollFreq(x);
  }
  
  function rollFreq(x) {
    let outcome = document.createTextNode(diceRecord[possibleNumbers[x]] + ";    ");
    document.body.appendChild(outcome);
  }
}

function printDiceGraph() {
  const possibleNumbers = Object.keys(diceRecord);
  let printH2 = document.createElement("h2");
  document.body.appendChild(printH2);

  let graph = document.createElement("div");
  graph.classList.add("graph");
  // added CSS class above
  document.body.appendChild(graph);

  for (let x = 0; x < possibleNumbers.length; x++) {
    let bar = document.createElement("div");
    bar.classList.add("bar");
    bar.setAttribute("style", "width: " + diceRecord[possibleNumbers[x]] + "px;");
    graph.appendChild(bar);
  }
}

rollAndRecord();
printDiceRollRecord();
printDiceGraph();
